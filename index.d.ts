export enum BallotStatus {
  Wait = "Wait",
  Open = "Open",
  Closed = "Closed"
}

export enum Unit {
  Hour = "Hour",
  Day = "Day",
  Week = "week",
  Month = "Month"
}

export default interface Duration {
  length: number;
  units: Unit;
}

export interface User {
  _id?: string;
  name: string;
  email: string;
  pfp_url: string;
}

export interface AuthUser extends User {
  api_keys: string[];
}

export interface Team {
  _id?: string;
  name: string;
  color: string;
  logo_url: string;
}

export interface League {
  _id?: string;
  name: string;
  owner: User;
  teams: Team[];
}

export interface VoterTicket {
  _id?: string;
  voter: User;
  ranks: Team[];
}

export interface Ballot {
  _id?: string;
  due: Date;
  count: number;
  status: BallotStatus;
  tickets: VoterTicket[];
}

export interface Poll {
  _id?: string;
  name: string;
  owner: User;
  league: League;
  ballots: Ballot[];
  voters: User[];
}
